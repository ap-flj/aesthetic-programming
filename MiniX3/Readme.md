# MiniX3 - We Got All Day...

![](MiniX3/MiniX3pic.png "We Got All Day")

[Go to wait here](https://ap-flj.gitlab.io/aesthetic-programming/MiniX3)

[View full repository](https://gitlab.com/ap-flj/aesthetic-programming/-/tree/745e0a53e22e550f0c47c60320bd76433b850c1f/MiniX3)

In this MiniX I have tried expressing the passing of time in different ways, within the same scenario.
First the sun appeared, out of the code we got from Lecture 09. The sun has been a parameter of timekeeping as long as anyone has been around to keep time. 
Then came the clouds. Growing up, one of the most boring things I could imagine was watching the clouds; they moved sooo sloooooowly. With them, I spent time experimenting with how fast they moved along the x-axis, and how their speed and direction changed the feel of the screen. 
Unfortunately the sky isn't always blue, and I also played around with how to mark the part of the day I most often find dragging by with not only the background colour, but also the layering of the objects. At first I struggled with layered trails, but when I finally figured how to make them dissappear, I found the layered look described better how time sometimes can be experienced, to me at least. 


When I encounter a throbber, I usually get impatient instantly. It means something isn't working. Or so is my first thought. I'm so used to it being a bad thing, that it can make my mood drop instantly. And it is boring. Nothing is happening! My favourite throbber has to be the one in DSB's app. A little train going round, and round, and round. I still get impatient, but it takes more time, and I somewhat enjoy looking at the little train. If HBO did the same when the internet connection is bad, I would curse them less, I think. 
I have tried reflecting this instant impatience and fear of boredom in my own throbber, by removing the cursor, and perhaps slightly confusing the viewer. The text "We got all day..." sends a message that this may take a long time, while also devaluing the time of the viewer. The layered effect of the movement mimics slowmotion effects, further adding to the metaphor of time passing. 

### References
[RGB Colour Chart](https://www.rapidtables.com/web/color/RGB_Color.html) 

[Codingtrain: Bouncing Ball](https://www.youtube.com/watch?v=LO3Awjn_gyU&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=15) 

[p5 Get Started](https://p5js.org/get-started/) 

[p5 Program Flow](https://p5js.org/learn/program-flow.html)

