//throbber
let x = 0;
let a = 0;
let b = 1000;
let c = 300;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(10);
  //remove cursor, remove stress
  noCursor();
  }

  function draw() {
   //the alpha value affects how the ellipses "fade" with the movement
  drawElements();
  }
  function drawElements() {
  let num = 9;

    push();
    //change of background
    if(mouseX >= width / 4 && mouseX < width / 2) {
      background(0,128,226,100);
      } else if(mouseX >= width /2 && mouseX < width*0.75) {
        background(26,0,255,30);
      } else if(mouseX >= width*0.75){
        background(255,47,68,100);
      } else {
        background(70,213,255,100);
      }
   // Text
    fill('white');
    textSize(50);
    text('We got all day...',500,200);

    if(mouseX >= width /2 && mouseX < width*0.75){
      fill('white');
      textSize(25);
     text('time moves slow...',800,500);
    }
    
  //move things to the center, and make the sun follow the mouse
    translate(mouseX, height / 2);
    noStroke();
    fill(255, 255, 0);
    ellipse(0,0,50);
  
    let cir = 360 / num * (frameCount % num);
    rotate(radians(cir));
    noStroke();
    triangle(35,0,60,10,35,20);
    pop();

    push();
      // clouds
      noStroke();
      fill(255, 255, 255);
   
      

      ellipse(x + 40, windowHeight / 4, 75);
      ellipse(x, windowHeight / 4 - 8, 100);
      ellipse(x - 40 , windowHeight / 4, 75);


      if(x < windowWidth){
        x = x + 4;
      } else {
        x = 0;
        }
  
      ellipse(a + 25, windowHeight / 8, 45);
      ellipse(a, windowHeight / 8 - 4, 60);
      ellipse(a - 25, windowHeight / 8, 45);

      if(a < windowWidth){
        a = a + 9;
      } else {
        a = 0;
        }

        ellipse(b + 30, windowHeight / 2, 50);
        ellipse(b, windowHeight / 2 - 4, 70);
        ellipse(b - 30, windowHeight / 2, 50);

        if(b > 0){
          b = b -7;
        } else {
          b = 1250;
          }

        ellipse(c + 50, windowHeight / 1.1, 90);
        ellipse(c, windowHeight / 1.1 - 4, 130);
        ellipse(c - 50, windowHeight / 1.1, 90);

        if(c > 0){
          c = c -2;
        } else {
          c = 1250;
          }

      pop();
  
  }
  function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  }
