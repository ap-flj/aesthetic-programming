# MiniX5 - Draw Like Dali

___
### Program Picture Coming
___

[Run Me](https://ap-flj.gitlab.io/aesthetic-programming/MiniX5)


The assignment dictated we start out by making some simple rules. I chose to make things difficult for myself:

**Rule 1: The generated art should not be predictable**

**Rule 2: The generated art should stop after a certain time**

My code determs that while the elapsed time is less than 10 seconds, a point should be made a random place on the canvas. To create a smooth line, and not a sea of dots, I used the _"noise()"_ funktion. While the "random()" function returns a number within a given range, the "noise()" function always returns a number between 0 and 1 (the amount can be scaled), creating a smother shift, rather than an abrupt jump. 

![](MiniX5/Salvador-Dali-Automatic-Drawing.jpg "Automatic drawing by Salvador Dali 1927")

My code is inspired by the automatism techniques used by the surrealist movement. Automatic drawing is a technique in which the artist draws _without thinking_, to try and instead let the subconcious speak. These drawings sometime ressemble random scribbles, while others are abstractly figurative. For some of the early surrealists, just drawing without thinking wasn't enough. To create true automatic art, they claimed, one had to be out of the conscious to get into the subconcious. Some artists practiced this by taking a nap with pen and paper in their hands, and then when waking first thing, just start drawing, before their mind was really awake. Others would take it further, knocking themselves out, like Salvador Dali, who was known for doing handstands until he fell unconcious. 
<br>
To avoid knocking myself out, I thought, if one seeks the truly random and unpredictable, a computer should be able to do the trick. It wont accidentally start drawing something figurative or start thinking about what it is doing. It is good at following rules (like **_don't think_** or in this case; **_be totally unpredictable_**). 
<br>
While some of the early surrealists would have been very impressed by my coded autogerated art, some of the hardcore theorists, like Dali, would probably deem it out, since it won't be a true reflection of my subconcious. However, it could raise the question: _"Do computers have a subconcious?"_. Or perhaps leaning into the very modern disscussion of AI and art; _if the rules of automatism is to not think, why wouldn't a computer do it as well as Dali? Can AI make better art than humans as long as we define clear rules for what art is? And can one really define rules for art?_
<br>
The surrealists were certainly very good at creating very many, very complicated rules, for something supposedly about not thinking. But art can also be a very poleising subject. 

### References:
[Using Timers](http://learn.digitalharbor.org/courses/creative-programming/lessons/using-timers-in-p5-js/)

[Podcast Vanvittig Verdenshistorie on surrealism: ](https://open.spotify.com/episode/7buVcsGXG41c86F4cLcdDx?si=9348e0da3117465a "Syrehovedernes kunstkamp (LIVE fra ARoS)")