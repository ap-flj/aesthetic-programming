

let timer;
let stopTimer = 10;

function setup() {
createCanvas(windowWidth,windowHeight);

background(255,249,213);


}

function draw() {

timer = millis() / 1000;
// print(timer); 

stroke('black');
strokeWeight(5);

for( let i = 0; i < stopTimer; i += timer) {
    x = 500 * noise(0.005 * frameCount);
    y = 500 * noise(0.005 * frameCount + 10000);
    point(x,y);
}

if (timer > stopTimer) {
    print("stop");
    noLoop();
    strokeWeight(3);
    textSize(30);
    textFont('Papyrus');
    text('Salvador Dali',windowWidth/4,windowHeight-windowHeight/4);
}
}

