# MiniX Mini House

![MiniX](Skærmbillede_2024-06-07_120611.png
"MiniX Mini House")
<br>

[Run me here](https://ap-flj.gitlab.io/aesthetic-programming/MiniX)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->
<br>
Please view the full repository [here](https://gitlab.com/ap-flj/aesthetic-programming/-/tree/main/MiniX?ref_type=heads)


