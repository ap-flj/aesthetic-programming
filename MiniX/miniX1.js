let y = 130;
let speed = 70;
let windowColours = ['rgb(255,179,0)','rgb(223,136,21)','rgb(169,96,0)'];

function setup() {
 // put setup code here
 createCanvas(500,500,);
 

}

function draw() {
  // put drawing code rehe
  background(22,4,84);
  frameRate(10);
  //House
  fill(160,80,0);
  stroke("black")
 rect(100,300,300,150);

 line(100,440,400,440);
 line(100,430,400,430);
 line(100,420,400,420);
 line(100,410,400,410);
 line(100,400,400,400);
 line(100,390,400,390);
 line(100,380,400,380);
 line(100,370,400,370);
 line(100,360,400,360);
 line(100,350,400,350);
 line(100,340,400,340);
 line(100,330,400,330);
 line(100,320,400,320);
 line(100,310,400,310);


//Roof
 fill(233,19,19);
 triangle(250,150,420,300,80,300);
 //Chimney
 let deltaY = speed - deltaTime;
 y += deltaY

 fill(44,38,32);
 quad(300,150,350,150,350,250,300,200);
 
 //Door
 rect(250,350,70,100);
 fill("grey")
 ellipse(260,400,10)

 //Window
 fill(random(windowColours))
 square(150,350,50);

 fill("black")
 line(150,375,200,375);
 line(175,350,175,400);
 

 function smoke(){

  noStroke();
  fill(255,255,255);
  ellipse(340,y-5,25);
  ellipse(320,y,25);
  ellipse(340,y-15,25);
  ellipse(320,y-15,25);

  ellipse(350,y-50,25);
  ellipse(340,y-55,25);
  ellipse(350,y-60,25);
  ellipse(340,y-70,25);

   ellipse(320,y+20,25);
   ellipse(310,y+25,25);
   ellipse(320,y+30,25);
   ellipse(310,y+35,25);
  
    if (y < 0) {
   y =130;
   };
}
smoke()

}
