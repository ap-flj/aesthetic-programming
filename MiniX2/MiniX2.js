
//Define variable for the colour of the emoji
let emojiSliderFarve;


function setup() {
 // create canvas and render it to be able to use 3D effects
 createCanvas(windowWidth,windowHeight,WEBGL);
 // createSlider(min,max,defaultValue,stepSize)
 
 emojiSliderFarve = createSlider(50,255,150);
 emojiSliderFarve.position(475,450);
 emojiSliderFarve.size(200);
 
}

function draw() {
  
  background("lightBlue");
  let emojiFarve = emojiSliderFarve.value();
  // lanyard
  noStroke();
  fill(7,125,3);
  circle(-160,60,110);
  // face
  fill(0,0,emojiFarve);
  circle(-100,0,100);
  // eyes
  fill('white');
  ellipse(-120,-10,20);
  ellipse(-80,-10,20);
  fill(0,0,emojiFarve,);
  ellipse(-120,-5,20);
  ellipse(-80,-5,20);
 
  angleMode(DEGREES);
  noFill();
  //mouth
  stroke('red');
  strokeWeight(3);
  arc(-100,10,60,50,0,180);
  //flower outline
  stroke(255,162,0);
  strokeWeight(2);
  //Sunflower
  fill('yellow');
  ellipse(-160,60,15,100);
  ellipse(-160,60,100,15);

  push();
  angleMode(DEGREES);
  translate(-160,60);
  rotate(125);
  ellipse(0,0,90,15);
  rotate(100);
  ellipse(0,0,90,15); 
  pop(); 

  push();
  fill('brown');
  ellipse(-160,60,20);
  pop();
  
}
