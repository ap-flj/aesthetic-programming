#  Minix2 - Repressentation Matters

Inclusion and repressentation matters. In this MiniX I started out by reflecting on what emojis I felt missing. As a white woman with blond hair I hardly miss physical repressentation in emojis. What I however need for repressentation is something that includes those of us who lives with invisible dissabilities. 
Both wheelchairusers as well as amputees have gotten emojis, but how do you condense something so fluffy and *unseeable* as **invisible** dissabilities? 

A symbol I myself use when I need to signal my disabillity is the sunflowerlanyard of the [Sunflower Project](https://hdsunflower.com/us/). The Sunflower Project is an international organization working to make invisible disabilities more visible by using the sunflower as a signal. Wearing a green sunflowerlanyard signals to others that you have a disability and might need more time than others. 
Therefore I have made a simple emoji, with a sunflower. To skip around the big discussion of skincolour, I have made my emoji blue. The nuance of blue can be changed by the slider

![Repressentation Matters](MiniX2/sunflowerEmoji.png "Sunflower Emoji")

[Run Me Here](https://ap-flj.gitlab.io/aesthetic-programming/MiniX2)
